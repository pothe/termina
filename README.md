# Termina = Terminal + Página Web💻

Termina crea páginas web como si fueran terminales con comandos funcionales.

![](https://i.imgur.com/wqaqeNB.png)

# ¿Porque Termina?
Es una bifucarción de un script llamado Termpage y que a su vez es una terminal dentro de una página web.

# ¿Cómo usar?
Descargando este repositorio en formato .zip.

Usando esta etiqueta en tu index.html
```html
<script src="https://cdn.jsdelivr.net/npm/termpage@0.1.1/dist/termpage.min.js" type="text/javascript">
</script>
```

Descargar mediante Git
```https://pothe@bitbucket.org/pothe/termina.git```

Esta es la forma más fácil de tener tu propia Termina poniendo este código en tu index.html
```html
<!doctype html>
<html>
  <head>
    <title>termpage@home</title>
    <script src="https://cdn.jsdelivr.net/npm/termpage@0.1.1/dist/termpage.min.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="termpage-window" style="position:absolute;top:0;bottom:0;left:0;right:0" id="window"></div>
    <script>
      Termpage.init(document.getElementById('window'), (command) => {
        command = command.toLowerCase().trim();
        if (command === 'inicio') {
          return "Bienvenido a Termina";
        } else {
          return 'Comando no encontrado';
        }
      }, {
        initialCommand: 'inicio'
      });
    </script>
  </body>
</html>
```
Encontrarás demos más complicadas en la [Carpeta de Ejemplos](https://bitbucket.org/pothe/termina/src/master/ejemplos/)

# Documentación de la API

### Termpage.init

`Termpage.init(domElement, commandParser, options)`

```javascript
commandParser = (input) => {
  return response;
}
```

`input` es cuando el usuario escribe un comando en la terminal

`response` es una respuesta en string u objeto `{text: responseText, commands: ['menuCommand1', 'menuCommand']}`

El arreglo de comandos es para los dispositivos móviles y solo necesiten tocar los comandos en vez de escribirlos en la pantalla.

Lista de opciones disponibles:

```javascript
  {
    prompt: '$',
    initialCommand: 'home',
    autoFocus: true
  },
```

Las respuestas pueden ser regresadas como **promesas** para comandos asicronicos.

### Termpage.replace, Termpage.link y Termpage.color

`Termpage.replace` junto con `Termpage.link` y `Termpage.color` son ayudantes para mantener el estilo de terminal sin perder la alineación entre lineas.

Este codigo es para imprimir celdas con color
```javascript
Termpage.replace(
`
------------------
| Celda1 | Celda2 |
------------------
`, {Celda1: Termpage.link('https://bitbucket.org/pothe/termina'), Celda2: Termpage.color('red')})
```

# Estilos

Estos son los estilos por defecto de Termina pero puedes modificarlos con tus archivos css.

```css
.termpage-window {
  background-color: black;
  border: 2px solid #888;
  padding-top: 5px;
}

.termpage-window * {
  font-family: "Courier New", Courier, monospace;
  font-size: 16px;
  color: #ddd;
}

.termpage-input {
  background-color: #222;
  color: #ddd;
  caret-color: white;
}

.termpage-block, .termpage-input {
  line-height: 20px;
}

.termpage-block {
  padding-left: 5px;
  padding-right: 5px;
}

.termpage-window a {
  background-color: #888;
  text-decoration: none;
  cursor:pointer;
}
.termpage-window a:hover {
  background-color: #333;
}

.termpage-menu {
  background-color: #888;
}

.termpage-menu li:hover {
  background-color: #666;
  cursor: pointer;
}
```
